# LICENSE CDDL 1.0 + GPL 2.0
#
# Copyright (c) 2017-2018 TmaxSoft and/or its affiliates. All rights reserved.
#
# TmaxSoft DOCKERFILES PROJECT
# --------------------------
# This is the Dockerfile for TmaxSoft JEUS Server 7.0.0.4 Product Distro
#
# HOW TO BUILD THIS IMAGE
# -----------------------
# Put all downloaded files in the same directory as this Dockerfile
# Run:
#      $ docker build -f Dockerfile.jeus8-domain -t javaprosky/ubuntu-jeus8-domain-k8s .
#
# IMPORTANT
# ---------
# The resulting image of this Dockerfile contains a JEUS Empty Domain.
#
# Pull base image
# From the TmaxSoft Registry
# -------------------------

FROM javaprosky/centos-jeus8-domain

# Maintainer
# ----------
MAINTAINER DongJin Shin <dongjin_shin@tmax.co.kr>

# Common environment variables required for this build (do NOT change)
# --------------------------------------------------------------------
USER tmax

WORKDIR ${TMAX_HOME}

# Define default command to start script.
#CMD ["/home/tmax/run.sh"]
CMD ["/home/tmax/start.sh"]
